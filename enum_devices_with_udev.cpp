// g++ --std=c++0x -lasound -ludev
#include <alsa/asoundlib.h>
#include <libudev.h>

const char * const  SUBSYSTEM_NAME = "sound";

void after_snd_ctl_open(
    snd_ctl_t * _ctl
    , int       _device
)
{
    snd_pcm_info_t *    pcmInfo;

    snd_pcm_info_alloca( &pcmInfo );
    snd_pcm_info_set_device(
        pcmInfo
        , _device
    );
    snd_pcm_info_set_subdevice(
        pcmInfo
        , 0
    );
    snd_pcm_info_set_stream(
        pcmInfo
        , SND_PCM_STREAM_CAPTURE
    );

    int result = snd_ctl_pcm_info(
        _ctl
        , pcmInfo
    );
    if( result != 0 ) {
        printf( "failed snd_ctl_pcm_info [%d]\n", result );
        return;
    }

    const char *    NAME = snd_pcm_info_get_name( pcmInfo );
    printf( "name : %s\n", NAME );
}

void print_device_data(
    struct udev_device *    _device
)
{
    const char *    DEVPATH = udev_device_get_devpath( _device );
    printf( "devpath : %s\n", DEVPATH );

    const char *    DEVNODE = udev_device_get_devnode( _device );
    printf( "devnode : %s\n", DEVNODE );

    const char *    NODE = strrchr( DEVPATH, '/' );
    if( NODE == nullptr ) {
        return;
    }

    int     card = -1;
    int     device = -1;
    char    type;
    sscanf(
        NODE
        , "/pcmC%dD%d%c"
        , &card
        , &device
        , &type
    );

    if( card < 0 ) {
        return;
    }

    if( device < 0 ) {
        return;
    }

    if( type == 'c' ) {
        return;
    }

    printf( "card number : %d\n", card );
    printf( "device number : %d\n", device );

    char    cardName[ 128 ];
    snprintf( cardName, sizeof( cardName ), "hw:%d", card );

    snd_ctl_t * ctl;
    int result = snd_ctl_open(
        &ctl
        , cardName
        , 0
    );
    if( result != 0 ) {
        printf( "failed snd_ctl_open [%d]\n", result );
        return;
    }

    after_snd_ctl_open(
        ctl
        , device
    );

    snd_ctl_close( ctl );
}

int after_udev_enumerate_new(
    struct udev *               _udev
    , struct udev_monitor *     _monitor
    , struct udev_enumerate *   _enumerate
)
{
    int result;

    result = udev_enumerate_add_match_subsystem(
        _enumerate
        , SUBSYSTEM_NAME
    );
    if( result != 0 ) {
        printf( "failed udev_enumerate_add_match_subsystem [%d]\n", result );
        return -5;
    }

    result = udev_monitor_enable_receiving( _monitor );
    if( result != 0 ) {
        printf( "failed udev_monitor_enable_receiving [%d]\n", result );
        return -6;
    }

    result = udev_enumerate_scan_devices( _enumerate );
    if( result != 0 ) {
        printf( "failed udev_enumerate_scan_devices [%d]\n", result );
        return -7;
    }

    struct udev_list_entry *    list = udev_enumerate_get_list_entry( _enumerate );
    struct udev_list_entry *    entry;
    int                         i = 0;
    udev_list_entry_foreach( entry, list )
    {
        printf( "---------- %d ----------\n", i );
        i++;

        const char *    SYSPATH = udev_list_entry_get_name( entry );

        printf( "syspath : %s\n", SYSPATH );

        struct udev_device *    device = udev_device_new_from_syspath(
            _udev
            , SYSPATH
        );

        print_device_data(
            device
        );

        udev_device_unref( device );
    }

    return 0;
}

int after_udev_monitor_new_from_netlink(
    struct udev *           _udev
    , struct udev_monitor * _monitor
)
{
    int result;

    result = udev_monitor_filter_add_match_subsystem_devtype(
        _monitor
        , SUBSYSTEM_NAME
        , nullptr
    );
    if( result != 0 ) {
        printf( "failed udev_monitor_filter_add_match_subsystem_devtype [%d]\n", result );
        return -3;
    }

    struct udev_enumerate * enumerate = udev_enumerate_new( _udev );
    if( enumerate == nullptr ) {
        printf( "failed udev_enumerate_new\n" );
        return -4;
    }

    result = after_udev_enumerate_new(
        _udev
        , _monitor
        , enumerate
    );

    udev_enumerate_unref( enumerate );

    if( result != 0 ) {
        return result;
    }

    struct pollfd   ufd;
    ufd.fd = udev_monitor_get_fd( _monitor );
    ufd.events = POLLIN;

    printf( "デバイスの監視を開始(終了はCtrl+c)\n" );

    while( 1 ) {
        while( poll( &ufd, 1, -1 ) == -1 ) {
            printf( "!!\n" );
            if( errno != EINTR ) {
                break;
            }
        }

        struct udev_device *    device = udev_monitor_receive_device( _monitor );

        const char *    ACTION = udev_device_get_action( device );
        if( strcmp( ACTION, "add" ) == 0 ) {
            print_device_data(
                device
            );
        }

        udev_device_unref( device );
    }

    return result;
}

int after_udev_new(
    struct udev *   _udev
)
{
    struct udev_monitor *   monitor = udev_monitor_new_from_netlink(
        _udev
        , "udev"
    );
    if( monitor == nullptr ) {
        printf( "failed udev_monitor_new_from_netlink\n" );
        return -2;
    }

    int result = after_udev_monitor_new_from_netlink(
        _udev
        , monitor
    );

    udev_monitor_unref( monitor );

    return result;
}

int main(
)
{
    struct udev *   udev = udev_new();
    if( udev == nullptr ) {
        printf( "failed udev_new\n" );
        return -1;
    }

    int result = after_udev_new(
        udev
    );

    udev_unref( udev );

    return result;
}
