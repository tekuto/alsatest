// g++ -lasound
#include <alsa/asoundlib.h>
#include <stdio.h>

int main(
)
{
    int card = -1;

    while( snd_card_next( &card ) == 0 ) {
        if( card == -1 ) {
            printf( "end\n" );
            break;
        }

        char *  name;
        snd_card_get_name(
            card
            , &name
        );
        printf( "%d: %s\n", card, name );
        free( name );
    }
}
