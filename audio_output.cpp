// g++ --std=c++0x -lasound
#include <alsa/asoundlib.h>
#include <stdio.h>
#include <stdint.h>

const char    MAGIC_STR_RIFF[] = { 'R', 'I', 'F', 'F' };
const char    MAGIC_STR_WAVE[] = { 'W', 'A', 'V', 'E' };

const char    TAG_STR_FMT[] = { 'f', 'm', 't', ' ' };
const char    TAG_STR_DATA[] = { 'd', 'a', 't', 'a' };

struct RiffHeader
{
    char    magic[ 4 ]; // 'R' 'I' 'F' 'F'
    int32_t fileSize;
};

struct WavHeader
{
    char    magic[ 4 ]; // 'W' 'A' 'V' 'E'
};

struct RiffChunkHeader
{
    char    tag[ 4 ];
    int32_t chunkSize;
};

struct FmtChunk
{
    int16_t formatId;
    int16_t channelsCount;
    int32_t sampleRate;
    int32_t bytesPerSec;
    int16_t blockSize;
    int16_t bitsPerSample;
};

bool readData(
    FILE *      _file
    , void *    _buffer
    , size_t    _bufferSize
)
{
    if( fread(
        _buffer
        , _bufferSize
        , 1
        , _file
    ) != 1 ) {
        printf( "failed fread\n" );
        return false;
    }

    return true;
}

bool after_alloc_buffer(
    snd_pcm_t *         _handle
    , FILE *            _file
    , char *            _buffer
    , int               _bufferSize
    , snd_pcm_uframes_t _frames
)
{
    while( 1 ) {
        auto    readSize = fread(
            _buffer
            , 1
            , _bufferSize
            , _file
        );
        if( readSize == 0 ) {
            break;
        }

        if( readSize < _bufferSize ) {
            memset(
                _buffer + readSize
                , 0
                , _bufferSize - readSize
            );
        }

        auto    result = snd_pcm_writei(
            _handle
            , _buffer
            , _frames
        );
    }

    snd_pcm_drain( _handle );
}

bool checkRiffHeader(
    FILE *  _file
)
{
    RiffHeader  header;
    if( readData(
        _file
        , &header
        , sizeof( header )
    ) == false ) {
        return false;
    }

    if( memcmp( header.magic, MAGIC_STR_RIFF, sizeof( MAGIC_STR_RIFF ) ) != 0 ) {
        printf( "failed RIFF magic check [%.*s]\n", ( int )sizeof( header.magic ), header.magic );
        return false;
    }

    return true;
}

bool checkWavHeader(
    FILE *  _file
)
{
    WavHeader   header;
    if( readData(
        _file
        , &header
        , sizeof( header )
    ) == false ) {
        return false;
    }

    if( memcmp( header.magic, MAGIC_STR_WAVE, sizeof( MAGIC_STR_WAVE ) ) != 0 ) {
        printf( "failed WAVE magic check [%.*s]\n", ( int )sizeof( header.magic ), header.magic );
        return false;
    }

    return true;
}

bool readFmtChunk(
    FILE *          _file
    , FmtChunk &    _fmtChunk
)
{
    RiffChunkHeader header;
    if( readData(
        _file
        , &header
        , sizeof( header )
    ) == false ) {
        return false;
    }

    if( memcmp( header.tag, TAG_STR_FMT, sizeof( TAG_STR_FMT ) ) != 0 ) {
        printf( "failed fmt chunk tag check [%.*s]\n", ( int )sizeof( header.tag ), header.tag );
        return false;
    }

    char    buffer[ header.chunkSize ];
    if( readData(
        _file
        , buffer
        , header.chunkSize
    ) == false ) {
        return false;
    }

    memcpy(
        &_fmtChunk
        , buffer
        , sizeof( _fmtChunk )
    );

    return true;
}

bool checkDataChunkHeader(
    FILE *  _file
)
{
    RiffChunkHeader header;
    if( readData(
        _file
        , &header
        , sizeof( header )
    ) == false ) {
        return false;
    }

    if( memcmp( header.tag, TAG_STR_DATA, sizeof( TAG_STR_DATA ) ) != 0 ) {
        printf( "failed data chunk tag check [%.*s]\n", ( int )sizeof( header.tag ), header.tag );
        return false;
    }

    return true;
}

bool after_fopen(
    snd_pcm_t * _handle
    , FILE *    _file
)
{
    if( checkRiffHeader( _file ) == false ) {
        printf( "failed checkRiffHeader\n" );
        return false;
    }

    if( checkWavHeader( _file ) == false ) {
        printf( "failed checkWavHeader\n" );
        return false;
    }

    FmtChunk    fmtChunk;
    if( readFmtChunk( _file, fmtChunk ) == false ) {
        printf( "failed readFmtChunk\n" );
        return false;
    }

    if( checkDataChunkHeader( _file ) == false ) {
        printf( "failed checkDataChunkHeader\n" );
        return false;
    }

    printf( "fmt chunk data:\n" );
    printf( "\tformat ID : %d\n", fmtChunk.formatId );
    printf( "\tchannels count : %d\n", fmtChunk.channelsCount );
    printf( "\tsample rate : %d\n", fmtChunk.sampleRate );
    printf( "\tbytes per sec : %d\n", fmtChunk.bytesPerSec );
    printf( "\tblock size : %d\n", fmtChunk.blockSize );
    printf( "\tbits per sample : %d\n", fmtChunk.bitsPerSample );

    int result;

    snd_pcm_hw_params_t *   hw;

    snd_pcm_hw_params_alloca( &hw );

    snd_pcm_hw_params_any(
        _handle
        , hw
    );

    snd_pcm_hw_params_set_access(
        _handle
        , hw
        , SND_PCM_ACCESS_RW_INTERLEAVED
    );

    snd_pcm_format_t    format;
    switch( fmtChunk.bitsPerSample ) {
    case 8: // 8bit
        format = SND_PCM_FORMAT_S8;
        break;

    case 16:    // 16bit
        format = SND_PCM_FORMAT_S16_LE;
        break;

    default:
        printf( "サポート対象外のフォーマット\n" );
        return false;
        break;
    }
    snd_pcm_hw_params_set_format(
        _handle
        , hw
        , format
    );

    snd_pcm_hw_params_set_channels(
        _handle
        , hw
        , fmtChunk.channelsCount
    );

    unsigned int    sampleRate = fmtChunk.sampleRate;
    snd_pcm_hw_params_set_rate_near(
        _handle
        , hw
        , &sampleRate
        , nullptr
    );

    snd_pcm_uframes_t   frames = 32;
    snd_pcm_hw_params_set_period_size_near(
        _handle
        , hw
        , &frames
        , nullptr
    );

    result = snd_pcm_hw_params(
        _handle
        , hw
    );
    if( result != 0 ) {
        printf( "failed snd_pcm_hw_params [%d]\n", result );
        return false;
    }

    int     bufferSize = frames * fmtChunk.blockSize;
    char *  buffer = new char[ bufferSize ];

    bool    returnValue = after_alloc_buffer(
        _handle
        , _file
        , buffer
        , bufferSize
        , frames
    );

    delete [] buffer;

    return returnValue;
}

int after_snd_pcm_open(
    snd_pcm_t *     _handle
    , const char *  _FILE_NAME
)
{
    FILE *  file = fopen(
        _FILE_NAME
        , "r"
    );
    if( file == nullptr ) {
        printf( "failed fopen\n" );
        return -1;
    }

    bool    result = after_fopen(
        _handle
        , file
    );

    fclose( file );

    return result ? 0 : -1;
}

int main(
    int         _argc
    , char **   _argv
)
{
    if( _argc != 3 ) {
        printf( "使い方: %s 出力先 ファイル名\n", _argv[ 0 ] );
        return 0;
    }

    const char *    DEVICE_NAME = _argv[ 1 ];
    const char *    FILE_NAME = _argv[ 2 ];

    int result;

    snd_pcm_t * handle;
    result = snd_pcm_open(
        &handle
        , DEVICE_NAME
        , SND_PCM_STREAM_PLAYBACK
        , 0
    );
    if( result != 0 ) {
        printf( "failed snd_pcm_open [%d]\n", result );
        return -1;
    }

    result = after_snd_pcm_open(
        handle
        , FILE_NAME
    );

    snd_pcm_close( handle );

    return result;
}
