// g++ --std=c++0x -lasound
#include <alsa/asoundlib.h>

int after_snd_device_name_hint(
    const void * const *    _HINTS
)
{
    for( int i = 0 ; _HINTS[ i ] != nullptr ; i++ ) {
        const void *    HINT = _HINTS[ i ];

        printf( "---------- %d ----------\n", i );

        char *  type = snd_device_name_get_hint(
            HINT
            , "IOID"
        );
        printf( "\ttype : %s\n", type == nullptr ? "Both" : type );
        free( type );

        char *  name = snd_device_name_get_hint(
            HINT
            , "NAME"
        );
        printf( "\tname : %s\n", name );
        free( name );

        char *  desc = snd_device_name_get_hint(
            HINT
            , "DESC"
        );
        printf( "\tdescription : %s\n", desc );
        free( desc );
    }

    return 0;
}

int main(
)
{
    int result;

    void ** hints;

    result = snd_device_name_hint(
        -1
        , "pcm"
        , &hints
    );
    if( result != 0 ) {
        fprintf(
            stderr
            , "snd_device_name_hint error [%d]\n"
            , result
        );
        return -1;
    }

    result = after_snd_device_name_hint(
        hints
    );

    snd_device_name_free_hint( hints );

    return result;
}
